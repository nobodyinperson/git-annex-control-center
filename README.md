# 🎛️ Git Annex Control Center

A TUI to manage git annex repos.

[![asciicast](https://asciinema.org/a/623450.svg)](https://asciinema.org/a/623450)


## Installation & Usage

If you have [the `nix` package manager](https://nixos.org) installed, you can run the control center directly as a flake:

```bash
nix run gitlab:nobodyinperson/git-annex-control-center
```

Otherwise, you can install this as any other Python package:

```bash
pip install git+https://gitlab.com/nobodyinperson/git-annex-control-center
# Installing with 'pipx' might be a better choice, though
```

## Development

```bash
# Make a venv and install the dependencies
poetry install

# Then, open *two* terminals/tmux panes.
# Terminal Nr. 1 (here you will see debug output, you can leave this open)
poetry run textual console

# Terminal Nr. 2 (for the app, CTRL-C and relaunch if you changed the code)
poetry run textual run git_annex_control_center/__main__.py
```

### Textual Web

You can use [`textual-web`](https://github.com/Textualize/textual-web) to serve the Git Annex Control Center to the web (Don't know why you would want that, but it works! Don't give anyone the link though, they get access to your maching through the control center!)

```bash
poetry run textual-web -r "python -m git_annex_control_center /path/to/repo"
```
