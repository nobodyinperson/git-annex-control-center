# system modules
import os
import webbrowser
import functools
from configparser import ConfigParser
import textwrap
import argparse
import subprocess
import shutil
import json
from pathlib import Path
from typing import Iterable

# internal modules

# external modules
from textual import on
from textual.binding import Binding
from textual.app import App, ComposeResult
from textual.reactive import reactive
from textual.screen import ModalScreen
from textual.command import Hit, Hits, Provider
from textual.widgets import (
    Header,
    Footer,
    Markdown,
    Static,
    Placeholder,
    TabbedContent,
    Switch,
    TabPane,
    DirectoryTree,
    Checkbox,
    Button,
)
from textual.containers import ScrollableContainer

from rich.syntax import Syntax
from rich.pretty import Pretty


# TODO: Config is not used currently. How to make it elegantly autosave e.g. on reactive variable changes?
class Config(Static):
    USERPATH = (
        Path(os.environ.get("XDG_CONFIG_HOME", Path("~/.config").expanduser()))
        / "git-annex"
        / "control-center.conf"
    )

    def __init__(self, *args, **kwargs):
        Static.__init__(self, *args, **kwargs)
        ConfigParser.__init__(self)

    def load(self):
        self.app.log("TODO: read config file {self.USERPATH}")

    def save(self):
        # make config dir if not present
        self.USERPATH.parent.mkdir(parents=True, exist_ok=True)
        with self.USERPATH.open("wt", encoding="utf-8", errors="ignore") as fh:
            self.app.log(f"Saving config to {fh.name!r}")
            self.write(fh)


class RemotesEditor(ScrollableContainer):
    def compose(self):
        yield Markdown(
            textwrap.dedent(
                """ 
            ## Remotes Editor

            In this editor for the remotes, I'd like to make the following things accessible and at best also **easier** than they currently are from the `git-annex` command-line interface or the assistant:

            - changing remotes' settings:
                - **description**
                    - suggest auto-generated description 
                - **group** 
                    - drop-down menu of existing groups
                    - We will also need a group editor. (editing `groupwanted`, adding new groups)
                - **wanted and required content**
                    - Ideally we'll make a UI with drag'n'droppable blocks for the [preferred content DSL](https://git-annex.branchable.com/preferred_content/). Or at least quick actions for things like `git annex groupwanted REMOTE present` or adding `approxlackingcopies=1`, things like that.

            ---------------------

            """
            ).strip()
        )
        git_annex = subprocess.Popen(
            ["git-annex", "info", "--json"],
            encoding="utf-8",
            errors="ignore",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        output, errors = git_annex.communicate()
        if output:
            yield Static(
                Syntax(
                    json.dumps(json.loads(output), sort_keys=True, indent=4),
                    lexer="json",
                    line_numbers=True,
                )
            )
        if errors:
            yield Static(errors)


class RepoSwitcher(Static):
    def compose(self) -> ComposeResult:
        yield Markdown(
            textwrap.dedent(
                """
                    ## Switching to other Git Annex Repos
                    
                    Eventually, I'd like this to be an overview of git annex
                    repos on this local machine with an ability to switch the
                    control center to them. These could include:

                    - repos that were once opened in this Control Center
                    - repos that are configured for the assistant to autostart
                    - repos visited in the shell - we could try to scan the shell history (is that evil 🤔?) 
                    - as a last-resort we could scan the filesystem (slow 🐌).
                    
                    ---------------------
                    """
            ).strip(),
        )


class RepoDirectoryTree(DirectoryTree):
    show_hidden_files = reactive(False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.show_hidden_files_wtf = (
            False  # TODO: This variable is a hack. See filter_paths()
        )

    def watch_show_hidden_files(self, show_hidden_files: bool) -> None:
        self.app.log(f"Reload files!")
        self.show_hidden_files_wtf = (
            self.show_hidden_files
        )  # TODO: This variable is a hack. See filter_paths()
        self.reload()

    def filter_paths(self, paths: Iterable[Path]) -> Iterable[Path]:
        for path in paths:
            # TODO: Can't use the reactive variable show_hidden_files here
            # directly for some reason. Whole app locks up with some worker
            # error. 🤷
            if not self.show_hidden_files_wtf and path.name.startswith("."):
                continue
            yield path


class FilesEditor(ScrollableContainer):
    def redo(self):
        self.query_one(DirectoryTree).path = Path("./").absolute()

    def compose(self):
        yield Markdown(
            textwrap.dedent(
                """ 
            ## Files Editor

            Here one will have access to the actual files in the repo. 
            I imagine the following features:

            - **moving files** around (`git annex get`, `drop`, `move`, `copy`)
            - simple **file preview** within this Control Center
            - editing **metadata**
                - switching into **views**
            - ...?

            ---------------------
            """
            ).strip()
        )
        yield Checkbox("show hidden files", id="show-hidden-files")
        yield RepoDirectoryTree(Path("./").absolute(), id="repofilestree")

    @on(Checkbox.Changed, "#show-hidden-files")
    def toggle_hidden_files(self, event: Checkbox.Changed) -> None:
        self.app.log(f"{event.checkbox.value = }")
        dirtree = self.app.query_one("#repofilestree", RepoDirectoryTree)
        dirtree.show_hidden_files = not dirtree.show_hidden_files


class Dashboard(ScrollableContainer):
    def compose(self):
        yield Markdown(
            textwrap.dedent(
                """ 
            ## Dashboard

            > ℹ️  Welcome to the **[Git Annex](https://git-annex.branchable.com) Control Center** "feature mockup"! 
            > You can't really do anything useful with it yet. But this is my
            > idea of what such a control center would look like and I plan to
            > gradually add features to it.
            > 
            > Feel free to message me [on Mastodon (`fosstodon.org/@nobodyinperson`)](https://fosstodon.org/@nobodyinperson) or to [open an issue on `gitlab.com/nobodyinperson/git-annex-control-center`](https://gitlab.com/nobodyinperson/git-annex-control-center).


            General goal of this Control Center:

            - **making typical Git Annex operations easier** by providing a "graphical" user interface.
                - reducing CLI fatigue for repetitive or tedious things (e.g. managing remotes, files, etc...)
                - optionally showing all `git annex ...` commands ran in the background so understanding and scripting it gets easier
                - providing pretty overviews of relevant information
            - **Onboarding** of new users.
                - Git Annex is undeniably a hard piece to grasp for command-line/Linux/git noobs. A simple graphical user interface definitely helps with the scary first steps.
                - There could be **inline documentation** in the Control Center. Tips here and there and links to the official docs.
            - (kinda) being **cross-platform**. [Textual](https://textual.textualize.io) (this TUI framework) is just Python and works pretty much everywhere. Git Annex itself also runs pretty much anywhere. I don't plan to add any fancy dependencies to this Control Center, and even if, I'd make them optional.
            - providing a (configurable?) utility for **personal Git Annex workflows**.
                - People use Git Annex in very different ways. This Control Center is surely biased towards my use cases. But it could be customizable with personal quick actions, etc.
                - There are [feature requests for Git Annex](https://git-annex.branchable.com/todo) that are too specific to be incorporated. These might make sense in a 3rd-party app like this.

            -----------------

            Things I'd like to be accessible from this dashboard:

            - quick settings for this repo
            - overviews
                - recent changes (who added/deleted/edited which file)
                - recent movements (which file was recently copied/moved/dropped from where and by whom if possible)
            - quick actions
                - syncing in various forms (`git annex sync` / `satisfy` / `assist`)
                - metadata actions (`git annex forget`)
                - consistency checks (`git annex fsck`)
                - ...?
            - repo stats
                - basically a pretty `git annex info`
                - the new repository size history as graphs?
                - ...?

            ---------------------
            """
            ).strip()
        )
        button = Button(
            "Set some .gitattributes", classes="unimplemented", id="set-gitattributes"
        )
        button.tooltip = "Set some useful git attributes for this local repo."
        yield button
        yield Static(f'{shutil.which("git-annex") = }')
        yield Static(f'{shutil.which("cowsay") = }')
        yield Static(
            f'{subprocess.check_output(["git-annex", "version"],stderr=subprocess.STDOUT).decode().strip()}'
        )


class HelpScreen(ModalScreen):
    def compose(self) -> ComposeResult:
        yield Markdown(
            textwrap.dedent(
                f"""
        # Help

        Press any key or click anywhere to close this help.

        ## ⌨️  Keybindings

        **TODO**: Auto-generated list of keybindings
        """
            ).strip()
        )

    def on_click(self, event):
        self.app.log(f"{event = }")
        self.app.pop_screen()

    def on_key(self, key):
        self.app.log(f"{key = }")
        self.app.pop_screen()


class GitAnnexControlCenterCommands(Provider):
    async def search(self, query: str) -> Hits:
        matcher = self.matcher(query)
        # TODO: yield something useful here


class GitAnnexControlCenter(App):
    """
    A Textual app to manage git annex repos
    """

    CSS_PATH = "git-annex-control-center.tcss"

    TITLE = "🎛️  Git Annex Control Center"

    COMMANDS = App.COMMANDS | {GitAnnexControlCenterCommands}

    BINDINGS = [
        # TODO: current dev Textual can apparently take a reactive as description to make it changeable.
        Binding("ctrl+d", "switch_pane('dashboard')", "Dashboard", show=False),
        Binding("ctrl+f", "switch_pane('fileseditor')", "Files", show=False),
        Binding("ctrl+r", "switch_pane('remoteseditor')", "Remotes", show=False),
        Binding("q", "quit", "Quit"),
        Binding("?", "show_help", "Help"),
        Binding("ctrl+u", "toggle_unimplemented", "Toggle Feature Preview"),
        Binding("ctrl+s", "screenshot", "Screenshot"),
        Binding("d", "toggle_dark", "light/dark"),
    ]

    repo = reactive(Path("./"))
    show_unimplemented = reactive(True)

    def watch_repo(self, repo: Path) -> None:
        os.chdir(str(repo))

    def __init__(
        self,
        args: argparse.Namespace = argparse.Namespace(),
    ) -> None:
        super().__init__()
        self.args = args
        self.repo = args.repo

    def action_quit(self) -> None:
        self.app.exit()

    def action_switch_pane(self, id: str) -> None:
        self.app.log(f"action_switch_pane({id = !r})")
        self.query_one("TabbedContent").active = id

    def action_show_help(self) -> None:
        self.push_screen(HelpScreen())

    def action_toggle_dark(self) -> None:
        self.dark = not self.dark

    def watch_show_unimplemented(self, state: bool):
        # TODO: TabPane's can't be hidden with CSS apparently, but need to be manually hidden/shown
        tabs = self.query_one(TabbedContent)
        getattr(tabs, "show_tab" if state else "hide_tab")("reposwitcher")
        # Show/hide unimplemented things via CSS
        for widget in self.app.query(".unimplemented"):
            if self.show_unimplemented:
                widget.remove_class("hidden")
            else:
                widget.add_class("hidden")

    def action_screenshot(self):
        screenshot = self.save_screenshot()
        self.notify(screenshot, title="📸 Screenshot saved ✅", severity="info")

    def action_toggle_unimplemented(self, state=None) -> None:
        self.show_unimplemented = not self.show_unimplemented
        # TODO: would be cool to change the action text between Show/Hide Feature Preview.
        # But apparently, one can't change a Binding's properties...

    @on(Markdown.LinkClicked)
    def open_link(self, event: Markdown.LinkClicked):
        self.app.log(f"Opening {event.href!r} in your browser")
        webbrowser.open(event.href)

    @on(Button.Pressed, ".unimplemented")
    def show_unimplemented_notification(self, event: Button.Pressed) -> None:
        # TODO: inelegant, should use namespace_bindings() here
        binding = next(
            b.key for b in self.BINDINGS if b.action == "toggle_unimplemented"
        )
        tooltip = f" ({tooltip})" if (tooltip := event.button.tooltip) else ""
        self.notify(
            textwrap.dedent(
                f"""
            [i]{event.button.label}[/i]{tooltip} is not yet implemented.

            🙈 Hide unimplemented features with [bold]{binding}[/bold]
            """
            ).strip(),
            title="❌ Not implemented",
            severity="warning",
        )

    def compose(self) -> ComposeResult:
        yield Header()
        yield Footer()
        with TabbedContent() as tabs:
            with TabPane("🚀 Dashboard", id="dashboard"):
                yield Dashboard(id="dashboard")
            with TabPane("📝 Files", id="fileseditor"):
                yield FilesEditor(id="fileseditor")
            with TabPane("🌐 Remotes", id="remoteseditor"):
                yield RemotesEditor(id="remoteseditor")
            with TabPane("📁 Other Repos", id="reposwitcher", classes="unimplemented"):
                yield RepoSwitcher(id="reposwitcher")

    def on_mount(self):
        if id := self.args.pane:
            # TODO: not elegant
            id = {"files": "fileseditor", "remotes": "remoteseditor"}.get(id, id)
            self.query_one(TabbedContent).active = id


def cli():
    parser = argparse.ArgumentParser(
        description="Manage your Git Annex repo in a TUI",
        prog="git annex control-center",
    )
    parser.add_argument("repo", help="repo to use", nargs="?", default="./")
    parser.add_argument(
        "--pane",
        help="starting pane to show upon opening",
        choices=["dashboard", "files", "remotes"],
    )
    args = parser.parse_args()
    app = GitAnnexControlCenter(args=args)
    app.run()


if __name__ == "__main__":
    cli()
